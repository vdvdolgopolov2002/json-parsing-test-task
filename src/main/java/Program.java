import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.*;

public class Program {

    public static void main(String[] args) {
        System.out.println("Введите полный путь до файла с билетами");

        try {

            FileReader fileReader = new FileReader(new BufferedReader(new InputStreamReader(System.in)).readLine());

            JsonStreamParser parser = new JsonStreamParser(fileReader);

            JsonElement ticketsJson = parser.next().getAsJsonObject().get("tickets");

            String originName = "Владивосток";
            String depZoneName = "Asia/Vladivostok";

            String destinationName = "Тель-Авив";
            String arrZoneName = "Asia/Tel_Aviv";

            Type ticketsListType = new TypeToken<List<Ticket>>() {}.getType();


            Map<String, Duration> carriersToMinDuration = new HashMap<>();
            List<Integer> prices = new ArrayList<>();

            List<Ticket> ticketList = new Gson().fromJson(ticketsJson, ticketsListType);

            for (Ticket ticket: ticketList) {

                if(originName.equals(ticket.getOrigin_name()) && destinationName.equals(ticket.getDestination_name())){
                    prices.add(ticket.getPrice());

                    if(carriersToMinDuration.containsKey(ticket.getCarrier())) {
                        if (carriersToMinDuration.get(ticket.getCarrier()).compareTo(ticket.getFlightDuration(depZoneName, arrZoneName)) > 0) {
                            carriersToMinDuration.replace(ticket.getCarrier(), ticket.getFlightDuration(depZoneName, arrZoneName));
                        }
                    }
                    else {
                        carriersToMinDuration.put(ticket.getCarrier(), ticket.getFlightDuration(depZoneName, arrZoneName));
                    }
                }
            }


            Collections.sort(prices);
            int size = prices.size();

            double arithmeticMean = prices.stream()
                    .mapToInt(Integer::intValue)
                    .average()
                    .orElse(0);


            double median;
            if (size % 2 == 0) {
                int middle1 = prices.get(size / 2);
                int middle2 = prices.get(size / 2 - 1);
                median = (middle1 + middle2) / 2.0;
            } else {
                median = prices.get(size / 2);
            }

            System.out.println("Разница между средней ценой и медианой: " + Math.abs(median - arithmeticMean));
            System.out.println("Билеты с минимальным временем полета из Владивостока в Тель-Авив");
            for (String carrier: carriersToMinDuration.keySet()) {
                System.out.println("Перевозчик: " + carrier + " Время полета: " + carriersToMinDuration.get(carrier).toHours() + " часов " +carriersToMinDuration.get(carrier).toMinutes() % 60 + " минут");
            }
            fileReader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
